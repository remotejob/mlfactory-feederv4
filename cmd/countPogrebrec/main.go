package main

import (
	"log"

	"github.com/akrylysov/pogreb"
)

func main() {
	dbp, err := pogreb.Open("pogreb", nil)
	if err != nil {
		log.Fatal(err)
	}
	defer dbp.Close()

	log.Println("count", dbp.Count())
}
