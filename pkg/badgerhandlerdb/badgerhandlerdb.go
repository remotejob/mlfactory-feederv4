package badgerhandlerdb

import (
	"github.com/dgraph-io/badger"
	config "gitlab.com/remotejob/mlfactory-feederv4/internal/configdb"
)

func InsertPage(conf *config.Config, txn *badger.Txn, uuid []byte, page []byte) {

	if err := txn.Set(uuid, page); err == badger.ErrTxnTooBig {
		if err := txn.Commit(); err != nil {
			panic(err)
		}
		txn = conf.BadgerDB.NewTransaction(true)
		if err := txn.Set(uuid, page); err != nil {
			panic(err)
		}
	}

}


func CountPages(conf *config.Config) (int, error) {

	var res int

	err := conf.BadgerDB.View(func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		opts.PrefetchValues = false
		it := txn.NewIterator(opts)
		defer it.Close()
		for it.Rewind(); it.Valid(); it.Next() {

			res = res + 1

		}
		return nil
	})

	return res, err
}


