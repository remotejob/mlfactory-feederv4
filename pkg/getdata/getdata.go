package getdata

import (
	"database/sql"

	"gitlab.com/remotejob/mlfactory-feederv4/pkg/dbhandler"
)

func GetOnlyOrg(db *sql.DB, stmt1 string, limit1 int) ([]string, error) {
	phrases, err := dbhandler.Get(db, stmt1, limit1)
	if err != nil {
		return nil, err
	}

	return phrases, nil

}

