package titlelinks

import (
	"math/rand"
	"strings"
	"time"

	"gitlab.com/remotejob/mlfactory-feederv4/internal/common"
	"gitlab.com/remotejob/mlfactory-feederv4/internal/domains"
)

func CreateForOnePage(mltitle string, orgphrases []string, activedom []string) domains.TitleLinks {

	actdomlen := len(activedom)

	nlinks := common.Random(5, 30)

	var set domains.TitleLinks

	title := strings.Title(mltitle)

	set.Title = title
	var links []string

	for n := 0; n < nlinks; n++ {


		longlink := orgphrases[common.Random(0, len(orgphrases))]
		secondphrasenom := common.Random(0, len(orgphrases))

		longlink = longlink + " " + orgphrases[secondphrasenom]

		lwords := strings.Fields(longlink)

		if len(lwords) > 8 {

			var shotlink string
			for i := 0; i < 8; i++ {

				shotlink = shotlink + " " + lwords[i]

			}

			longlink = shotlink

		} else {

			longlink = strings.Join(lwords, " ")
		}

		rand.Seed(time.Now().UTC().UnixNano())
		if rand.Intn(3) > 1 {

			dom := activedom[rand.Intn(actdomlen)]

			longlink = "http://" + dom + " " + longlink

		}
		// log.Println(longlink)
		links = append(links, strings.TrimSpace(longlink))

	}

	set.Links = links

	// res = append(res, set)

	// }

	return set

}

// func Create(orgphrases []string, npages int, activedom []string) []domains.TitleLinks {

// 	actdomlen := len(activedom)

// 	var res []domains.TitleLinks

// 	for i := 0; i < npages; i++ {

// 		nlinks := common.Random(5, 30)

// 		var set domains.TitleLinks

// 		title := strings.Title(orgphrases[i])

// 		set.Title = title
// 		var links []string

// 		for n := 0; n < nlinks; n++ {

// 			phrasenom := i + n

// 			longlink := orgphrases[phrasenom]
// 			secondphrasenom := common.Random(0, len(orgphrases))

// 			longlink = longlink + " " + orgphrases[secondphrasenom]

// 			lwords := strings.Fields(longlink)

// 			if len(lwords) > 8 {

// 				var shotlink string
// 				for i := 0; i < 8; i++ {

// 					shotlink = shotlink + " " + lwords[i]

// 				}

// 				longlink = shotlink

// 			} else {

// 				longlink = strings.Join(lwords, " ")
// 			}

// 			rand.Seed(time.Now().UTC().UnixNano())
// 			if rand.Intn(3) > 1 {

// 				dom := activedom[rand.Intn(actdomlen)]

// 				longlink = "http://" + dom + " " + longlink

// 			}
// 			// log.Println(longlink)
// 			links = append(links, strings.TrimSpace(longlink))

// 		}

// 		set.Links = links

// 		res = append(res, set)

// 	}

// 	return res

// }
