package dbhandler

import (
	"database/sql"
	// "log"
	"strconv"
)

func Get(db *sql.DB, stmt string, limit int) ([]string, error) {

	stmtfull := stmt + strconv.Itoa(limit)

	phrases := make([]string, limit)

	rows, err := db.Query(stmtfull)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var phrase string
	var count int

	for rows.Next() {
		rows.Scan(&phrase)
		phrases[count] = phrase
		count++
	}
	return phrases, nil
}
