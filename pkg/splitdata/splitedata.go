package splitdata

import (
	"strings"

	"gitlab.com/remotejob/mlfactory-feederv3/internal/common"
)

func Splite(in string) []string {

	var out []string
	nsenteses := strings.Split(in, ". ")

	nsentnum := len(nsenteses)
	var pagetxt string

	if nsentnum > 50 {

		count := common.Random(10, 30)

		for n, sen := range nsenteses {

			pagetxt = pagetxt + sen + ". "
			if n == count {

				count = common.Random(count+8, count+15)

				out = append(out, pagetxt)

				pagetxt = ""

			}

		}

	}

	return out
}
