package badgerhandler

import (
	"log"

	"github.com/dgraph-io/badger"
	// "gitlab.com/remotejob/mlfactory-feederv4/internal/config"
	"gitlab.com/remotejob/mlfactory-feederv4/internal/common"
	"gitlab.com/remotejob/mlfactory-feederv4/internal/config"
	"gitlab.com/remotejob/mlfactory-feederv4/internal/domains"
)

func InsertByBatch(conf *config.Config, items []domains.Item) {

	wb := conf.BadgerDB.NewWriteBatch()
	defer wb.Cancel()

	for _, item := range items {
		k, err := common.Buulid()
		if err != nil {
			log.Fatalln(err)
		}

		err =wb.Set(k,item.Page)
		if err != nil {
			log.Fatalln(err)
		}
	}
	err := wb.Flush()
	if err != nil {
		log.Fatalln(err)
	}
}

func InsertPage(conf *config.Config, txn *badger.Txn, uuid []byte, page []byte) {

	if err := txn.Set(uuid, page); err == badger.ErrTxnTooBig {
		if err := txn.Commit(); err != nil {
			panic(err)
		}
		txn = conf.BadgerDB.NewTransaction(true)
		if err := txn.Set(uuid, page); err != nil {
			panic(err)
		}
	}

}

func DeletePages(conf *config.Config, keys [][]byte) error {

	txn := conf.BadgerDB.NewTransaction(true)

	for _, k := range keys {

		if err := txn.Delete(k); err == badger.ErrTxnTooBig {
			if err := txn.Commit(); err != nil {
				panic(err)
			}
			txn = conf.BadgerDB.NewTransaction(true)
			if err := txn.Delete(k); err != nil {
				panic(err)
			}
		}

	}

	if err := txn.Commit(); err != nil {
		panic(err)
	}

	return nil

}

func CountPages(conf *config.Config) (int, error) {

	var res int
	// var err error

	err := conf.BadgerDB.View(func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		opts.PrefetchValues = false
		it := txn.NewIterator(opts)
		defer it.Close()
		for it.Rewind(); it.Valid(); it.Next() {
			// it.Item()

			res = res + 1
			// println("res",res)

		}
		return nil
	})

	return res, err

}

func GetPages(conf *config.Config, batchsize int) ([][]byte, [][]byte, error) {

	var key [][]byte
	var val [][]byte

	err := conf.BadgerDB.View(func(txn *badger.Txn) error {
		var count int
		opts := badger.DefaultIteratorOptions
		opts.PrefetchSize = 50
		it := txn.NewIterator(opts)
		defer it.Close()
		for it.Rewind(); it.Valid(); it.Next() {
			item := it.Item()
			k := item.Key()
			err := item.Value(func(v []byte) error {
				key = append(key, k)
				val = append(val, v)

				count = count + 1
				return nil
			})
			if err != nil {
				return err
			}

			if count > batchsize {
				break
			}
		}

		return nil
	})

	return key, val, err

}
