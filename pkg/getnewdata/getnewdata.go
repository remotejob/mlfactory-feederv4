package getnewdata

import (
	"encoding/json"
	"net/http"
	"time"

	"gitlab.com/remotejob/mlfactory-feederv4/internal/domains"
)

func GetRemoteData(link string) (domains.RemoteData, error) {

	var result domains.RemoteData
	myClient := &http.Client{Timeout: 10 * time.Second}
	resp, err := myClient.Get(link)
	if err != nil {

		return result, err
	}
	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {

		return result, err
	}


	return result,nil
}

func GetRemoteDatas(link string) ([]domains.RemoteData, error) {

	var result []domains.RemoteData
	myClient := &http.Client{Timeout: 30 * time.Second}
	resp, err := myClient.Get(link)
	if err != nil {

		return result, err
	}
	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {

		return result, err
	}


	return result,nil
}