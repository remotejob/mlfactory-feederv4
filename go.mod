module gitlab.com/remotejob/mlfactory-feederv4

go 1.13

require (
	github.com/akrylysov/pogreb v0.8.3
	github.com/dgraph-io/badger v1.6.0
	github.com/fsnotify/fsnotify v1.4.7
	github.com/gopherjs/gopherjs v0.0.0-20191106031601-ce3c9ade29de // indirect
	github.com/gosimple/slug v1.9.0
	github.com/julienschmidt/httprouter v1.3.0
	github.com/mattn/go-sqlite3 v2.0.1+incompatible
	github.com/oklog/ulid/v2 v2.0.2
	github.com/smartystreets/assertions v1.0.1 // indirect
	github.com/spf13/viper v1.6.1
	github.com/xujiajun/nutsdb v0.5.0
	gitlab.com/remotejob/collector v0.0.0-20191202014953-d7771a2cbe6a
	gitlab.com/remotejob/mlengine v1.0.0
	gitlab.com/remotejob/mlfactory-feederv3 v0.0.1
	golang.org/x/net v0.0.0-20191209160850-c0dbc17a3553 // indirect
	golang.org/x/sys v0.0.0-20191218084908-4a24b4065292 // indirect
)
